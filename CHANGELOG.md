# Changelog

## [5.0.2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/compare/5.0.1...5.0.2) (2024-10-01)


### Bug Fixes

* output variable value required index ([46cd60a](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/46cd60a2804b5122ada4d613d6328bb1ca2649fa))

## [5.0.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/compare/5.0.0...5.0.1) (2024-09-30)


### Bug Fixes

* uptime_check_auth_proxy is now indexed - move to prevent cycle errors ([31160ca](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/31160ca33d0e999f816d17e8ad89aa76de90a3aa))

## [5.0.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/compare/4.0.4...5.0.0) (2024-08-21)


### ⚠ BREAKING CHANGES

* deprecated variable alert_email_addresses removed

### Features

* deprecated variable alert_email_addresses removed ([797e417](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/797e417a51b24ed57f9a96303b461f07df2d1836))

## [4.0.4](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/compare/4.0.3...4.0.4) (2024-08-20)

## [4.0.3](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/compare/4.0.2...4.0.3) (2024-08-20)

## [4.0.2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/compare/4.0.1...4.0.2) (2024-08-08)


### Bug Fixes

* correct types of `map` variables ([2facca0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/2facca0a12c8a8658d6fef0beddcf3e2e100b7a7))
* set default TLS min age check to 30s (to match the README) ([852873d](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/852873dd6ae2d55b6fe0ddc2f6f4385025079556))

## [4.0.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/compare/4.0.0...4.0.1) (2024-07-24)

## [4.0.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/compare/3.2.0...4.0.0) (2024-07-17)


### ⚠ BREAKING CHANGES

* only use uptime_check_auth_proxy when needed

### Features

* add support for local_files_dir ([0a5a3bc](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/0a5a3bc49de9c7b07af193b0d268e6750c0af65d))
* only use uptime_check_auth_proxy when needed ([1e9305c](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/1e9305cfd55f0e204a223a7789d1cadf0e447b9f))

## [3.2.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/compare/3.1.1...3.2.0) (2024-02-14)


### Features

* enable auth proxy source bucket force destroy ([ca560ee](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/ca560ee3a994d9e26403a57b17e495551e9d69f3))


### Bug Fixes

* add outputs following refactor ([d5d36eb](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/d5d36eb96176d28b124b1f9c19330d1fb559ff15))

## [3.1.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/compare/3.1.0...3.1.1) (2024-02-05)


### Bug Fixes

* refactor auth-proxy module into main Terraform as this module was unnecessary ([84c5889](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/84c5889e5099723de4d414f0047604a0f13f0c79))

## [3.1.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/compare/3.0.1...3.1.0) (2024-01-26)


### Features

* bump gcp-function module to v2 ([135154e](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/135154ea5708b1577c6f14e44eb5c2a346e7d858))


### Bug Fixes

* define project variables explicitly ([e68c18e](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/e68c18e7baaa2d078435f5dd9ae04542b46c402c))
* replace google_project with google_client_config ([b54ec29](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/commit/b54ec29af32d1cf9acd59490aa72bb985aee3f6d))

## [3.0.1] - 2023-11-02

### Added

* Added support to publish to GitLab Terraform registry when tagged using semver

## [3.0.0] - 2022-04-26

### Changed

- Allow content matching as part of the uptime check config.
- Include an optional uptime check id in display names. This is useful when deploying multiple uptime checks
  for the same Cloud Run service and need to avoid name clashes.
- Allow specifying existing Notification Channels.

## [2.0.0] - 2022-05-17
### Changed
 - Updated to allow Terraform 1.x and Google provider 4.x
## [1.0.6] - 2021-11-02
### Changed
 - Trigger SSL alert when the certificate has 27 or less days remaining before expiry date.
   The old value (30 days) triggered some alarms when Google's *.run.app certificate ended up
   not being renewed until there was only 29 days left. Google documentation says:
   "About one month before expiry, the process to renew your certificate automatically begins"
   See https://cloud.google.com/load-balancing/docs/ssl-certificates/google-managed-certs#renewal

## [1.0.5] - 2021-07-16
### Changed
 - Added support to authentication proxy Cloud Function egress settings configuration.

## [1.0.4] - 2021-06-15
### Fixed
 - Fixed the alerting trigger percentage to correctly calculate the success
   percentage over the given uptime check period.

## [1.0.3] - 2021-06-03
### Changed
 - Changed alerting triggers to a percentage to reduce noise from occasional
   failed checks from multiple global testing sites.

## [1.0.2] - 2021-05-17
### Fixed
 - Fixed authenticated monitoring failing to apply with longer (>6 character)
   cloud run service names.

## [1.0.1] - 2021-03-09
### Changed
 - Normalise whitespace in filter strings to avoid terraform trying to make
   unnecessary changes.

## [1.0.0] - 2021-01-27
### Added
 - Initial version
