# GCP Site monitoring

This module provisions basic monitoring for a website. It consists of:

* An uptime check (optionally at a custom path)
* A TLS certificate expiry check

This is considered the minimum common monitoring for all sites.

## Versioning

The `master` branch contains the tip of development. The `v3` branch is the latest major version and will contain
non-breaking features and bug-fixes.

## Required Google Services

The following services must be enabled to use this module:

* cloudresourcemanager.googleapis.com
* monitoring.googleapis.com
* cloudfunctions.googleapis.com (only required if authentication_proxy is used)

## Examples

A minimal configuration of the module needs only specify a host and alerting email addresses. It
will assume that the web application is hosted within the default provider project. (See important
note below about Cloud Monitoring workspaces.)

```tf
module "monitoring" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-site-monitoring/devops"
  version = "~> 4.0"

  host                        = "www.example.com"
  alert_notification_channels = ["projects/[PROJECT_ID]/notificationChannels/[CHANNEL_ID]"]
}
```

One may further customise the module using various optional variables:

```tf
module "monitoring" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-site-monitoring/devops"
  version = "~> 4.0"

  # Required. Hostname of site to be monitored. Note the lack of "https://".
  host = "www.example.com"

  # Optional. Project id which hosts site.
  project = "..."

  # Optional. List of existing Notification Channels to be alerted.
  alert_notification_channels = ["projects/[PROJECT_ID]/notificationChannels/[CHANNEL_ID]"]

  # Optional. Parameters to customise uptime checks.
  uptime_check = {
    # Enable alerting. Default: true.
    alert_enabled = true

    # Optional. Short identifier to distinguish multiple checks for the same Cloud Run service.
    id = "healthz"

    # Path to check. Default: "/".
    path = "/healthz"

    # Timeout for uptime check. Default: 30 seconds.
    timeout = 60  # seconds

    # Period determining uptime check frequency. One of {60, 300, 600, 900}. Default: 300 seconds.
    period = 60  # seconds

    # The threshold of uptime check successes below which an alert will be raised. Default: 75.
    # This is calculated as a percentage of successful uptime checks over the uptime checking
    # period (specified above).
    success_threshold_percent = 75
  }

  # Optional. Parameters to enable/customise content matching.
  content_matchers = {
    content      = "true"
    matcher      = "MATCHES_JSON_PATH"
    json_path    = "$.all_ok"
    json_matcher = "EXACT_MATCH"
  }

  # Optional. Parameters to customise TLS certificate checks. Ignored when an
  # authentication proxy is used (TLS checks are disabled automatically).
  tls_check = {
    # Enable alerting. Default: true without authentication proxy,
    # otherwise false
    alert_enabled = true

    # Minimum age of certificate. Default is 30 days.
    minimum_age = 15  # days
  }

  # Optional. Allows a Cloud Function to be configured to authenticate and proxy
  # uptime checks to the underlying service. Allowing services which are secured
  # by service account authentication to be accessed by the monitoring service.
  # If this is provided, TLS checks cannot be completed as the monitoring service
  # does not have access to the certificate of the underlying service.
  authentication_proxy = {
    # Whether the authentication proxy is enabled. Default: false
    enabled                = true

    # The project that contains the cloud run service to proxy to.
    # Default: the value of the "project" variable
    cloud_run_project      = google_cloud_run_service.webapp.project

    # The region that contains the cloud run service to proxy to.
    cloud_run_region       = var.cloud_run_region

    # The name of the cloud run service to proxy to
    cloud_run_service_name = google_cloud_run_service.webapp.name

    # Optional. Timeout for auth proxy function. Default: 30 seconds.
    timeout = 60 # seconds

    # If the service is internal, Cloud Function needs a VPC Connector in the same region.
    # to route the egress traffic through to reach the Cloud Run service
    egress_connector = var.egress_connector

    # Determines the egress traffic the Cloud Function routes through "var.egress_connector".
    # Can be one of either "ALL_TRAFFIC" or "PRIVATE_RANGES_ONLY". Defaults to "PRIVATE_RANGES_ONLY".
    egress_connector_settings = var.egress_connector_settings

    # Allow the authentication proxy function source code bucket to be forcibly
    # destroyed, even if the bucket still contains objects.
    source_bucket_force_destroy = false
  }
}
```

See [variables.tf](variables.tf) for all variables which can be set.

## Cloud Monitoring Workspaces

Note that the project containing resources to be monitored must be in a Cloud Monitoring workspace
which is configured by the `gcp-product-factory` for our standard products.

Cloud Monitoring distinguishes between workspaces and projects within those workspaces. Each
workspace must have a "scoping" project and that project *must* be the default project of the `google`
provider used by this module.

For example, the majority of the products managed by the DevOps team make use of a separate "meta"
project which is configured as the "scoping" project for all of the Google projects used by the
product's various environments. As such, use of this module usually requires a dedicated Google
provider which has rights to create monitoring-related resources on the "meta" project.

If the workspace scoping project differs from the project which contains the resources to be
monitored, you can use a provider alias:

```tf
provider "google" {
  project = "my-project"

  # ... some credentials for the *project* admin ...
}

provider "google" {
  project = "meta-project-hosting-cloud-monitoring-workspace"
  alias   = "monitoring"

  # ... some credentials for the *product* admin ...
}

module "monitoring" {
  # ... other parameters ...

  providers = {
    google = google.monitoring
  }
}
```
