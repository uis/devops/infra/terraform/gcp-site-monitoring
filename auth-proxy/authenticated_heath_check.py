import json
import logging
import os
import urllib

from google.auth.transport import requests as google_requests
from google.oauth2 import id_token

LOG = logging.getLogger()
logging.basicConfig(level=logging.INFO)


def make_request(url: str):
    """
    # Cloud Run uses your service's hostname as the `audience` value
    # audience = 'https://my-cloud-run-service.run.app/'
    # The `url` is the full URL (hostname + path) receiving the request
    # url = 'https://my-cloud-run-service.run.app/my/awesome/url'
    """

    LOG.info(f"Making request to {url}")

    audience = urllib.parse.urljoin(url, "/")
    req = urllib.request.Request(url)
    auth_req = google_requests.Request()
    token = id_token.fetch_id_token(auth_req, audience)

    req.add_header("Authorization", f"Bearer {token}")
    res = urllib.request.urlopen(req)

    LOG.info(f"Successful response ({res.code}) from {url}")
    return res.read()


def main(*args, **kwargs):
    """
    Check the url on env["TARGET_URL"] returns a successful response.
    """

    url = os.environ["TARGET_URL"]

    result = make_request(url)

    return result, {"Content-Type": "application/json"}
