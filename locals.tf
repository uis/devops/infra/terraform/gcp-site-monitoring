# locals.tf defines local variables

# The google_client_config data source is used to get the default projects for the
# Google providers.
data "google_client_config" "current" {}

locals {
  display_name = (
    local.uptime_check.id == ""
    ? "${var.host} - ${terraform.workspace}"
    : "${var.host} - ${local.uptime_check.id} - ${terraform.workspace}"
  )

  # Merge authentication_proxy variable with default values.
  authentication_proxy = merge({
    cloud_run_project = local.project
  }, var.authentication_proxy)

  # Merge uptime_check variable with default values.
  # If auth proxy is enabled, substitute the host and path
  # for the host and path of the authentication proxy
  uptime_check = merge(
    {
      host = var.host
    },
    var.uptime_check,
    merge([
      for auth_proxy in module.uptime_check_auth_proxy : {
        host = split("/", trimprefix(auth_proxy.function.https_trigger_url, "https://"))[0]
        path = "/${split("/", trimprefix(auth_proxy.function.https_trigger_url, "https://"))[1]}"
      }
    ]...),
  )

  content_matchers = merge({
    content      = null
    matcher      = null
    json_path    = null
    json_matcher = null
  }, var.content_matchers)

  # Merge tls_check variable with default values, overriding it completely
  # to disable the tls check if the instance to be monitored requires
  # service account authentication
  tls_check = (
    local.authentication_proxy.enabled ?
    {
      alert_enabled = false
      minimum_age   = 0
    } :
    var.tls_check
  )

  # Use the default provider project if not provided in var.project.
  project = coalesce(var.project, data.google_client_config.current.project)

  # Use the local.project value if var.monitoring_scoping_project is not provided.
  monitoring_scoping_project = coalesce(var.monitoring_scoping_project, local.project)

  notification_channels = var.alert_notification_channels

  short_service_name = replace(substr(local.authentication_proxy.cloud_run_service_name, 0, 8), "/[-_\\s]+$/", "")
}
