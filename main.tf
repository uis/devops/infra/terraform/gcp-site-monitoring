# main.tf defines resources used to implement the module

# In this file we use trimspace() and replace() to normalise spacing for alert filter definitions.
# Google does this server side as well so if we didn't do it here, terraform would keep wanting to
# change the filters for values which are identical except for whitespace.

resource "google_monitoring_uptime_check_config" "https" {
  project      = local.monitoring_scoping_project
  display_name = local.display_name
  timeout      = "${local.uptime_check.timeout}s"
  period       = "${local.uptime_check.period}s"

  http_check {
    path         = local.uptime_check.path
    port         = 443
    use_ssl      = true
    validate_ssl = true
  }

  monitored_resource {
    type = "uptime_url"
    labels = {
      project_id = local.project
      host       = local.uptime_check.host
    }
  }

  dynamic "content_matchers" {
    for_each = local.content_matchers.content == null ? [] : ["enabled"]

    content {
      content = local.content_matchers.content
      matcher = local.content_matchers.matcher

      json_path_matcher {
        json_path    = local.content_matchers.json_path
        json_matcher = local.content_matchers.json_matcher
      }
    }
  }

  # workaround - see https://github.com/terraform-providers/terraform-provider-google/issues/3133
  lifecycle {
    create_before_destroy = true
  }
}

resource "google_monitoring_alert_policy" "uptime_alert" {
  project      = local.monitoring_scoping_project
  enabled      = local.uptime_check.alert_enabled
  display_name = "Uptime check for ${local.display_name}"

  notification_channels = local.notification_channels

  combiner = "OR"

  conditions {
    display_name = "Uptime check has run for ${local.display_name}"

    condition_absent {
      filter = trimspace(replace(
        <<-EOT
        metric.type="monitoring.googleapis.com/uptime_check/check_passed" AND
        metric.label.check_id="${google_monitoring_uptime_check_config.https.uptime_check_id}" AND
        resource.type="uptime_url"
        EOT
        , "\n", " "
      ))
      # absent conditions have to have a min duration of 2 minutes
      duration = "${max(local.uptime_check.period, 120)}s"

      aggregations {
        # absent conditions have to have a min duration of 2 minutes
        alignment_period     = "${max(local.uptime_check.period, 120)}s"
        cross_series_reducer = "REDUCE_COUNT"
        group_by_fields      = []
        per_series_aligner   = "ALIGN_COUNT"
      }

      trigger { count = 1 }
    }
  }

  conditions {
    display_name = "Uptime check passed for ${local.display_name}"

    condition_threshold {
      filter = trimspace(replace(
        <<-EOT
        metric.type="monitoring.googleapis.com/uptime_check/check_passed" AND
        metric.label.check_id="${google_monitoring_uptime_check_config.https.uptime_check_id}" AND
        resource.type="uptime_url"
        EOT
        , "\n", " "
      ))
      duration   = "${local.uptime_check.period}s"
      comparison = "COMPARISON_LT"

      threshold_value = local.uptime_check.success_threshold_percent / 100

      aggregations {
        alignment_period     = "${local.uptime_check.period}s"
        cross_series_reducer = "REDUCE_FRACTION_TRUE"
        per_series_aligner   = "ALIGN_NEXT_OLDER"
      }

      trigger { count = 1 }
    }
  }
}

resource "google_monitoring_alert_policy" "ssl_cert_expiry" {
  project               = local.monitoring_scoping_project
  count                 = local.tls_check.alert_enabled ? 1 : 0
  enabled               = true
  display_name          = "SSL expiry check for ${var.host} (${terraform.workspace})"
  notification_channels = local.notification_channels

  combiner = "OR"
  conditions {
    display_name = "SSL expiry check for ${var.host} (${terraform.workspace})"

    condition_threshold {
      filter = trimspace(replace(
        <<-EOT
        metric.type="monitoring.googleapis.com/uptime_check/time_until_ssl_cert_expires" AND
        metric.label.check_id="${google_monitoring_uptime_check_config.https.uptime_check_id}" AND
        resource.type="uptime_url"
        EOT
        , "\n", " "
      ))
      duration   = "60s"
      comparison = "COMPARISON_LT"

      threshold_value = local.tls_check.minimum_age

      trigger { count = 1 }
    }
  }
}

module "uptime_check_auth_proxy" {
  count = local.authentication_proxy.enabled ? 1 : 0

  source  = "gitlab.developers.cam.ac.uk/uis/gcp-function/devops"
  version = "~> 2.1"

  project      = local.authentication_proxy.cloud_run_project
  region       = local.authentication_proxy.cloud_run_region
  trigger_http = true

  egress_connector          = local.authentication_proxy.egress_connector
  egress_connector_settings = local.authentication_proxy.egress_connector_settings

  name        = "${local.short_service_name}-uptime"
  description = <<-EOT
    A function which proxies authentication for the uptime check to ${var.host}"
  EOT

  allow_unauthenticated_invocations = true
  source_bucket_force_destroy       = local.authentication_proxy.source_bucket_force_destroy

  runtime = "python312"
  source_files = {
    "requirements.txt" = <<-EOT
        google-auth==2.32.0
        requests==2.32.3
    EOT
    "main.py"          = file("${path.module}/auth-proxy/authenticated_heath_check.py")
  }

  timeout = local.authentication_proxy.timeout

  environment_variables = {
    TARGET_URL = "https://${trimsuffix(var.host, "/")}/${trimprefix(coalesce(var.uptime_check.path, "/"), "/")}"
  }

  local_files_dir = var.local_files_dir
}

resource "google_cloud_run_service_iam_member" "uptime_check_invoker" {
  count = local.authentication_proxy.enabled ? 1 : 0

  project  = local.authentication_proxy.cloud_run_project
  location = local.authentication_proxy.cloud_run_region
  service  = local.authentication_proxy.cloud_run_service_name
  role     = "roles/run.invoker"
  member   = "serviceAccount:${module.uptime_check_auth_proxy[count.index].service_account.email}"
}
