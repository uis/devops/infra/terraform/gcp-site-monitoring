moved {
  from = module.authentication_proxy[0].google_cloud_run_service_iam_member.uptime_check_invoker
  to   = google_cloud_run_service_iam_member.uptime_check_invoker
}

moved {
  from = module.authentication_proxy[0].module.uptime_check_auth_proxy
  to   = module.uptime_check_auth_proxy
}

moved {
  from = module.uptime_check_auth_proxy
  to   = module.uptime_check_auth_proxy[0]
}
