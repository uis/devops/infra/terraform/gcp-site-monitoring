output "https_uptime_check_config" {
  description = "The configuration of the HTTPS uptime check."
  value       = google_monitoring_uptime_check_config.https
}

output "uptime_check_alert_policy" {
  description = "Details of the uptime check alert policy."
  value       = google_monitoring_alert_policy.uptime_alert
}

output "ssl_cert_expiry_alert_policy" {
  description = "Details of the SSL certificate expiry alert policy."
  value       = try(google_monitoring_alert_policy.ssl_cert_expiry[0], "")
}

output "auth_proxy_function" {
  description = "Details of the authentication proxy function."
  value       = try(module.uptime_check_auth_proxy[0].function, "")
}

output "auth_proxy_host" {
  description = "The authentication proxy host."
  value       = try(split("/", trimprefix(module.uptime_check_auth_proxy[0].function.https_trigger_url, "https://"))[0], "")
}

output "auth_proxy_path" {
  description = "The authentication proxy path."
  value       = try("/${split("/", trimprefix(module.uptime_check_auth_proxy[0].function.https_trigger_url, "https://"))[1]}", "")
}
