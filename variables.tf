# variables.tf defines variables used by the module

variable "host" {
  type        = string
  description = <<EOL
    Required. Hostname of site to be monitored. Do *NOT* include any URL parts such as a leading
    "https://" or a trailing slash.
EOL
}

variable "project" {
  description = <<EOL
    Optional. The ID of the project containing the var.host site to be monitored. The project must be part of a Cloud
    Monitoring workspace and the google provider must have rights to manage monitoring resources within that workspace.
    If not specified the default provider project must be set as this will be used instead.
EOL
  type        = string
  default     = null
}

variable "monitoring_scoping_project" {
  description = <<EOL
    The ID of a Cloud Monitoring scoping project to create alert policies and uptime checks in.
    If not set local.project is used instead.
EOL
  type        = string
  default     = null
}

variable "alert_notification_channels" {
  type        = list(string)
  default     = []
  description = <<EOL
  Optional. A list of notification channel IDs to send alerts to. The format for the channel IDs should
  be as follows.

  [
    "projects/[PROJECT_ID]/notificationChannels/[CHANNEL_ID]"
  ]
EOL
}

variable "local_files_dir" {
  description = <<-EOT
    A local directory where files may be created which persist between runs but
    which are not checked into source control.
    This variable is only used to be passed to the gcp-function module call.
  EOT
  type        = string
  default     = null
}

variable "uptime_check" {
  type = object({
    id                        = optional(string, "")
    alert_enabled             = optional(bool, true)
    path                      = optional(string, "/")
    timeout                   = optional(number, 30)
    period                    = optional(number, 300)
    success_threshold_percent = optional(number, 75)
  })
  default     = {}
  description = "Optional. Configuration for uptime checks. See README."
}

variable "content_matchers" {
  type = object({
    content      = optional(string)
    matcher      = optional(string)
    json_path    = optional(string)
    json_matcher = optional(string)
  })
  default     = {}
  description = "Optional. Configuration for content matching. See README."
}

variable "tls_check" {
  type = object({
    alert_enabled = optional(bool, true)
    minimum_age   = optional(number, 30)
  })
  default     = {}
  description = "Optional. Configuration for TLS checks. See README."
}

variable "authentication_proxy" {
  type = object({
    enabled = optional(bool, false)

    # Default is set in `locals.tf` as the default is calculated when
    # generating the plan
    cloud_run_project = optional(string)

    # The following two elements must be assigned non-empty string values
    # when the authentication proxy is enabled.
    cloud_run_region       = optional(string, "")
    cloud_run_service_name = optional(string, "")

    timeout          = optional(number, 30)
    egress_connector = optional(string, "")
    # Default null value of `egress_connector_settings` corresponds to
    # `PRIVATE_RANGES_ONLY`.
    egress_connector_settings   = optional(string)
    source_bucket_force_destroy = optional(bool)
  })
  default     = {}
  description = "Optional. Configuration for an authentication proxy. See README."
}
