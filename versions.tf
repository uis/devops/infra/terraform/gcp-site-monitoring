# versions.tf specifies minimum versions for providers and terraform.

terraform {
  # Specify the required providers, their version restrictions and where to get
  # them.
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.54"
    }
  }

  # Minimum terraform version
  required_version = ">= 0.13"
}
